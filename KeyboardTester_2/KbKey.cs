﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyboardTester_2
{
    class KbKey
    {
        public string Name { get; set; }
        public bool IsPressed { get; set; }

        public KbKey(string name)
        {
            this.Name = name;
            this.IsPressed = false;
        }
    }
}
