﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace KeyboardTester_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Dictionary<string, bool> kb_keys = new Dictionary<string, bool>();
        Dictionary<string, TextBlock> textboxes_Small = new Dictionary<string, TextBlock>();
        Dictionary<string, TextBlock> textboxes_L = new Dictionary<string, TextBlock>();
        Dictionary<string, TextBlock> textboxes_R = new Dictionary<string, TextBlock>();
        Dictionary<string, TextBlock> textboxes_Num = new Dictionary<string, TextBlock>();
        Dictionary<string, TextBlock> textboxes_NumBlock = new Dictionary<string, TextBlock>();
        Dictionary<string, TextBlock> textboxes_PMarks = new Dictionary<string, TextBlock>();
        Dictionary<string, TextBlock> textboxes_F = new Dictionary<string, TextBlock>();
        Dictionary<string, TextBlock> textboxes_System = new Dictionary<string, TextBlock>();
        string remarkNumLock = "NumLock is enabled. Some keys do not work. To disable, please press NumLock on your keyboard.";
        string remarkF10 = "F10 is enabled. Some keys possibly do not work. To disable, please press F10.";
        string result = "";

        bool wereAltPressed = false;
        bool wereAltGrPressed = false;
        bool wereF10pressed = false;
        bool wereEnterMainPressed = false;
        bool wereEnterNumPressed = false;

        public MainWindow()
        {
            InitializeComponent();
            MessageBox.Show("Please first check the keys above the arrow-keys - Home, PageUp, Delete, End and PageDown.");
            CreateKeys();
            FillTBLists();
            if (Keyboard.IsKeyToggled(Key.NumLock) == false)
            {
                Remark_TB.Text = remarkNumLock;
            }
            else Remark_TB.Text = "";
        }        
        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            string date = DateTime.Now.ToShortDateString();
            result = "Keyboard has been tested on " + date + "\r\n" +
                  "\r\nCapital letters with left shift: " + Checked_TB_LCap.Text + "\r\n" +
                  "Capital letters with right shift: " + Checked_TB_RCap.Text + "\r\n" +
                  "Small letters: " + Checked_TB_Small.Text + "\r\n" +
                  "Numbers: " + Checked_TB_Num.Text + "\r\n" +
                  "Numbers on the NumBlock: " + Checked_TB_NumBlock.Text + "\r\n" +
                  "Punctuation marks: " + Checked_TB_PMarks.Text + "\r\n" +
                  "System keys: " + Checked_TB_Sys.Text + "\r\n\r\n\r\n\r\n" +
                  "Tested by:________________________________(signature) ";
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text file (*.txt)|*.txt";
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            saveFileDialog.FileName = "KeyboardTest";
            if (saveFileDialog.ShowDialog() == true)
                File.WriteAllText(saveFileDialog.FileName, result);
        }

        private void Clear_Button_Click(object sender, RoutedEventArgs e)
        {
            //Checked_TB.Text = "";
            resetTbForeground(textboxes_F);
            resetTbForeground(textboxes_L);
            resetTbForeground(textboxes_Num);
            resetTbForeground(textboxes_NumBlock);
            resetTbForeground(textboxes_PMarks);
            resetTbForeground(textboxes_R);
            resetTbForeground(textboxes_Small);
            resetTbForeground(textboxes_System);
            Checked_TB_LCap.Text = "";
            Checked_TB_RCap.Text = "";
            Checked_TB_Small.Text = "";
            Checked_TB_Num.Text = "";
            Checked_TB_NumBlock.Text = "";
            Checked_TB_PMarks.Text = "";
            Checked_TB_Sys.Text = "";
            List<string> keyList = new List<string>(kb_keys.Keys);
            foreach (string k in keyList)
            {
                kb_keys[k] = false;
            }
        }
        public void resetTbForeground(Dictionary<string, TextBlock> dict)
        {
            foreach (KeyValuePair<string, TextBlock> entry in dict)
            {
                entry.Value.Foreground = new SolidColorBrush(Colors.Black);
            }
        }
        private void Info_Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Christoph Högi & Alexey Orlov, 2017.");
        }
        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            string key = e.Key.ToString();
            string keyCode = e.SystemKey.ToString();
            bool f10Toggled = Keyboard.IsKeyToggled(Key.F10);
            if (key == "NumLock")
                    {
                        if (Keyboard.IsKeyToggled(Key.NumLock) == false)
                        {
                            Remark_TB.Text = remarkNumLock;
                        }
                        else Remark_TB.Text = "";
                    }
            if(key == "F10")
                {
                    if (!Keyboard.IsKeyToggled(Key.F10))
                    {
                        RemarkF10_TB.Text = remarkF10;
                    }
                    else RemarkF10_TB.Text = "";
                }
            if (!SysKeysChecked() &&
                (key != "Insert" &&
                key != "Home" &&
                key != "PageUp" &&
                key != "Delete" &&
                key != "End" &&
                key != "Next" ))
            {
                List<string> SysKeyList = new List<string>();
                string nonCheckedSysKeys = "Following keys must be checked first: \r\n\r\n";
                SysKeyList.Add("Home");
                SysKeyList.Add("PageUp");
                SysKeyList.Add("Delete");
                SysKeyList.Add("End");
                SysKeyList.Add("Next");
                foreach (string item in SysKeyList)
                {
                    if (kb_keys[item] == false)
                        nonCheckedSysKeys += item + "\r\n";
                }
                MessageBox.Show(nonCheckedSysKeys);
            }
            else
            {
                if (Keyboard.IsKeyDown(Key.LeftShift) && !Keyboard.IsKeyDown(Key.RightShift))
                {
                    key = "L_Cap" + key;
                }
                else if (Keyboard.IsKeyDown(Key.RightShift) && !Keyboard.IsKeyDown(Key.LeftShift))
                {
                    key = "R_Cap" + key;
                }
                if (kb_keys.ContainsKey(key))
                {
                    if (kb_keys[key] == false)
                    {
                        if (key.Contains("L_Cap"))
                        {
                            Checked_TB_LCap.Text += key[key.Length - 1] + " ";
                            textboxes_L[key.ToString()].Foreground = new SolidColorBrush(Colors.White);

                        }
                        else if (key.Contains("R_Cap"))
                        {
                            Checked_TB_RCap.Text += key[key.Length - 1] + " ";
                            textboxes_R[key.ToString()].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key.Length == 1)
                        {
                            Checked_TB_Small .Text += key.ToLower() + " ";
                            textboxes_Small[key.ToString().ToUpper()].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key.Length == 2 && key[0] == 'D')
                        {
                            Checked_TB_Num.Text += key[1] + " ";
                            textboxes_Num[key.ToString()].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Multiply")
                        {
                            Checked_TB_NumBlock.Text += "* ";
                            textboxes_NumBlock["Multiply"].Foreground = new SolidColorBrush(Colors.White);

                        }
                        else if (key == "Divide")
                        {
                            Checked_TB_NumBlock.Text += "/ ";
                            textboxes_NumBlock["Divide"].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Subtract")
                        {
                            Checked_TB_NumBlock.Text += "- ";
                            textboxes_NumBlock["Subtract"].Foreground = new SolidColorBrush(Colors.White);

                        }
                        else if (key == "Add")
                        {
                            Checked_TB_NumBlock.Text += "+ ";
                            textboxes_NumBlock["Add"].Foreground = new SolidColorBrush(Colors.White);

                        }
                        else if (key == "Return" && !IsExtended(e)) //Main Enter-key (underneath the Backspace)
                        {
                            if (wereEnterMainPressed == false)
                            {
                                Checked_TB_Sys.Text += "Enter ";
                                textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                                wereEnterMainPressed = true;
                            }
                        }
                        else if (key == "Return" && IsExtended(e)) //Enter-key on the NumBlock
                        {
                            if (wereEnterNumPressed == false)
                            {
                                Checked_TB_NumBlock.Text += "Enter(NumPad) ";
                                NumBlockEnter_TB.Foreground = new SolidColorBrush(Colors.White);
                                wereEnterNumPressed = true;
                            }
                        
                        }
                        else if (key == "NumLock")
                        {
                            Checked_TB_NumBlock.Text += "NumLock ";
                            textboxes_NumBlock["NumLock"].Foreground = new SolidColorBrush(Colors.White);
                            if (Keyboard.IsKeyToggled(Key.NumLock) == false)
                            {
                                Remark_TB.Text = remarkNumLock;
                            }
                            else Remark_TB.Text = "";

                        }
                        else if (key.Contains("NumPad") )//&& Keyboard.IsKeyToggled(Key.NumLock) == false
                        {
                            if (Keyboard.IsKeyToggled(Key.NumLock))
                            {
                                Checked_TB_NumBlock.Text += key[key.Length - 1] + " ";
                                textboxes_NumBlock[key].Foreground = new SolidColorBrush(Colors.White);
                            }
                        }
                        else if (key.Contains("Decimal"))
                        {
                            Checked_TB_NumBlock.Text += ". ";
                            textboxes_NumBlock[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if(key == "OemComma") //Punctuation marks
                        {
                            Checked_TB_PMarks.Text += ", ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "OemPeriod")
                        {
                            Checked_TB_PMarks.Text += ". ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Oem1")
                        {
                            Checked_TB_PMarks.Text += "; ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Oem3")
                        {
                            Checked_TB_PMarks.Text += "' ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Oem5")
                        {
                            Checked_TB_PMarks.Text += "\\ ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Oem6")
                        {
                            Checked_TB_PMarks.Text += "] ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Oem8")
                        {
                            Checked_TB_PMarks.Text += "` ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "OemQuestion")
                        {
                            Checked_TB_PMarks.Text += "/ ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "OemMinus")
                        {
                            Checked_TB_PMarks.Text += "- ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "OemPlus")
                        {
                            Checked_TB_PMarks.Text += "= ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "OemOpenBrackets")
                        {
                            Checked_TB_PMarks.Text += "[ ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "OemQuotes")
                        {
                            Checked_TB_PMarks.Text += "# ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Space")
                        {
                            Checked_TB_PMarks.Text += "Spacebar ";
                            textboxes_PMarks[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if(key == "System" && keyCode == "F10")
                        {
                            if(wereF10pressed == false)
                            {
                                Checked_TB_Sys.Text += "F10 ";
                                textboxes_F["F10"].Foreground = new SolidColorBrush(Colors.White);
                                wereF10pressed = true;
                            }                        
                        }
                        else if (key.Length > 1 && key.Length < 4 && key[0] == 'F')
                        {
                            Checked_TB_Sys.Text += key + " ";
                            textboxes_F[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if(key == "Escape")
                        {
                            Checked_TB_Sys.Text += key + " ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Snapshot")
                        {
                            Checked_TB_Sys.Text += "PrintScreen ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Scroll")
                        {
                            Checked_TB_Sys.Text += "ScrollLock ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Pause")
                        {
                            Checked_TB_Sys.Text += key + " ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Tab")
                        {
                            Checked_TB_Sys.Text += key + " ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Capital")
                        {
                            Checked_TB_Sys.Text += "CapsLock ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);                        
                        }
                        else if (key == "LeftShift")
                        {
                            Checked_TB_Sys.Text += "Left Shift ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "RightShift")
                        {
                            Checked_TB_Sys.Text += "Right Shift ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Back")
                        {
                            Checked_TB_Sys.Text += "Backspace ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                    
                        else if (key == "LeftCtrl")
                        {
                            Checked_TB_Sys.Text += "Left Control ";
                            textboxes_System["LeftCtrl"].Foreground = new SolidColorBrush(Colors.White);
                        }
                    
                        else if (key == "RightCtrl")
                        {
                            Checked_TB_Sys.Text += "Right Control ";
                            textboxes_System["RightCtrl"].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "RWin")
                        {
                            Checked_TB_Sys.Text += "Right Win ";
                            textboxes_System["RWin"].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "LWin")
                        {
                            Checked_TB_Sys.Text += "Left Win ";
                            textboxes_System["LWin"].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "System" && keyCode == "LeftAlt") // || keyCode == "LeftAlt"
                        {
                            Checked_TB_Sys.Text += "Alt ";
                            textboxes_System["Alt"].Foreground = new SolidColorBrush(Colors.White);
                            wereAltPressed = true;
                            if (wereAltGrPressed && wereAltPressed)
                                kb_keys[key] = true;
                        }
                        else if (key == "System" && keyCode == "LeftCtrl") //|| keyCode == "RightAlt"
                        {
                            Checked_TB_Sys.Text += "AltGr ";
                            textboxes_System["AltGr"].Foreground = new SolidColorBrush(Colors.White);
                            wereAltGrPressed = true;
                            if(wereAltGrPressed && wereAltPressed)
                                kb_keys[key] = true;
                        }
                        else if (key == "Apps")
                        {
                            Checked_TB_Sys.Text += key + " ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Insert" && IsExtended(e))
                        {
                            Checked_TB_Sys.Text += key + " ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Home" && IsExtended(e))
                        {
                            Checked_TB_Sys.Text += key + " ";
                               textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "PageUp" && IsExtended(e))
                        {
                            Checked_TB_Sys.Text += key + " ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Next" && IsExtended(e))
                        {
                            Checked_TB_Sys.Text += "Page Down ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Delete" && IsExtended(e))
                        {
                            Checked_TB_Sys.Text += key + " ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "End" && IsExtended(e))
                        {
                            Checked_TB_Sys.Text += key + " ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Up" && IsExtended(e))
                        {
                            Checked_TB_Sys.Text += "Arrow_up(↑) ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Down" && IsExtended(e))
                        {
                            Checked_TB_Sys.Text += "Arrow_down(↓) ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);                                               
                        }
                        else if (key == "Left" && IsExtended(e))
                        {
                            Checked_TB_Sys.Text += "Arrow_left(←) ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        else if (key == "Right" && IsExtended(e))
                        {
                            Checked_TB_Sys.Text += "Arrow_right(→) ";
                            textboxes_System[key].Foreground = new SolidColorBrush(Colors.White);
                        }
                        if (key != "System" && key != "Return")
                        kb_keys[key] = true;
                        if (Keyboard.IsKeyToggled(Key.F10))
                        {
                            RemarkF10_TB.Text = remarkF10;
                        }
                        else RemarkF10_TB.Text = "";
                    }
                }
            }
        }
        
        public void CreateKeys()
        {
            for (int i = 0; i <= 9; i++)
            {
                kb_keys.Add("Num" + i.ToString(), false);
            }
            for (char i = 'A'; i <= 'Z'; i++)
            {
                kb_keys.Add(i.ToString(), false);
            }
            for (char i = 'A'; i <= 'Z'; i++)
            {
                kb_keys.Add("R_Cap" + i, false);
            }
            for (char i = 'A'; i <= 'Z'; i++)
            {
                kb_keys.Add("L_Cap" + i, false);
            }
            for (int i = 0; i < 10; i++)
            {
                kb_keys.Add("D" + i, false);
            }
            for (int i = 0; i < 10; i++) // Adding the keys of NumBlock
            {
                kb_keys.Add("NumPad" + i, false);
            }
            kb_keys.Add("Multiply", false);
            kb_keys.Add("NumLock", false);
            kb_keys.Add("Divide", false);
            kb_keys.Add("Subtract", false);
            kb_keys.Add("Add", false);
            kb_keys.Add("Enter", false);
            kb_keys.Add("Decimal", false);

            kb_keys.Add("OemComma", false);
            kb_keys.Add("OemPeriod", false);
            kb_keys.Add("Oem1", false);
            kb_keys.Add("Oem3", false);
            kb_keys.Add("Oem5", false);
            kb_keys.Add("Oem6", false);
            kb_keys.Add("Oem8", false);
            kb_keys.Add("OemQuestion", false);
            kb_keys.Add("OemMinus", false);
            kb_keys.Add("OemPlus", false);
            kb_keys.Add("OemOpenBrackets", false);
            kb_keys.Add("OemQuotes", false);
            kb_keys.Add("Space", false);

            for (int i = 1; i < 20; i++)
            {
                kb_keys.Add("F" + i, false);
            }

            kb_keys.Add("Escape", false);
            kb_keys.Add("Snapshot", false);
            kb_keys.Add("Scroll", false);
            kb_keys.Add("Pause", false);
            kb_keys.Add("Tab", false);
            kb_keys.Add("Capital", false);
            kb_keys.Add("LeftShift", false);
            kb_keys.Add("RightShift", false);
            kb_keys.Add("Back", false);
            kb_keys.Add("Return", false);
            kb_keys.Add("LeftCtrl", false);
            kb_keys.Add("RightCtrl", false);
            kb_keys.Add("LWin", false);
            kb_keys.Add("RWin", false);
            kb_keys.Add("System", false);            
            kb_keys.Add("Apps", false);
            kb_keys.Add("Insert", false);
            kb_keys.Add("Home", false);
            kb_keys.Add("PageUp", false);

            kb_keys.Add("Next", false);
            kb_keys.Add("Delete", false);
            kb_keys.Add("End", false);
            kb_keys.Add("Up", false);
            kb_keys.Add("Left", false);
            kb_keys.Add("Right", false);
            kb_keys.Add("Down", false);
        }
        public void FillTBLists()
        {
            textboxes_L.Add("L_CapA", L_CapA_TB);
            textboxes_L.Add("L_CapB", L_CapB_TB);
            textboxes_L.Add("L_CapC", L_CapC_TB);
            textboxes_L.Add("L_CapD", L_CapD_TB);
            textboxes_L.Add("L_CapE", L_CapE_TB);
            textboxes_L.Add("L_CapF", L_CapF_TB);
            textboxes_L.Add("L_CapG", L_CapG_TB);
            textboxes_L.Add("L_CapH", L_CapH_TB);
            textboxes_L.Add("L_CapI", L_CapI_TB);
            textboxes_L.Add("L_CapJ", L_CapJ_TB);
            textboxes_L.Add("L_CapK", L_CapK_TB);
            textboxes_L.Add("L_CapL", L_CapL_TB);
            textboxes_L.Add("L_CapM", L_CapM_TB);
            textboxes_L.Add("L_CapN", L_CapN_TB);
            textboxes_L.Add("L_CapO", L_CapO_TB);
            textboxes_L.Add("L_CapP", L_CapP_TB);
            textboxes_L.Add("L_CapQ", L_CapQ_TB);
            textboxes_L.Add("L_CapR", L_CapR_TB);
            textboxes_L.Add("L_CapS", L_CapS_TB);
            textboxes_L.Add("L_CapT", L_CapT_TB);
            textboxes_L.Add("L_CapU", L_CapU_TB);
            textboxes_L.Add("L_CapV", L_CapV_TB);
            textboxes_L.Add("L_CapW", L_CapW_TB);
            textboxes_L.Add("L_CapX", L_CapX_TB);
            textboxes_L.Add("L_CapY", L_CapY_TB);
            textboxes_L.Add("L_CapZ", L_CapZ_TB);

            textboxes_R.Add("R_CapA", R_CapA_TB);
            textboxes_R.Add("R_CapB", R_CapB_TB);
            textboxes_R.Add("R_CapC", R_CapC_TB);
            textboxes_R.Add("R_CapD", R_CapD_TB);
            textboxes_R.Add("R_CapE", R_CapE_TB);
            textboxes_R.Add("R_CapF", R_CapF_TB);
            textboxes_R.Add("R_CapG", R_CapG_TB);
            textboxes_R.Add("R_CapH", R_CapH_TB);
            textboxes_R.Add("R_CapI", R_CapI_TB);
            textboxes_R.Add("R_CapJ", R_CapJ_TB);
            textboxes_R.Add("R_CapK", R_CapK_TB);
            textboxes_R.Add("R_CapL", R_CapL_TB);
            textboxes_R.Add("R_CapM", R_CapM_TB);
            textboxes_R.Add("R_CapN", R_CapN_TB);
            textboxes_R.Add("R_CapO", R_CapO_TB);
            textboxes_R.Add("R_CapP", R_CapP_TB);
            textboxes_R.Add("R_CapQ", R_CapQ_TB);
            textboxes_R.Add("R_CapR", R_CapR_TB);
            textboxes_R.Add("R_CapS", R_CapS_TB);
            textboxes_R.Add("R_CapT", R_CapT_TB);
            textboxes_R.Add("R_CapU", R_CapU_TB);
            textboxes_R.Add("R_CapV", R_CapV_TB);
            textboxes_R.Add("R_CapW", R_CapW_TB);
            textboxes_R.Add("R_CapX", R_CapX_TB);
            textboxes_R.Add("R_CapY", R_CapY_TB);
            textboxes_R.Add("R_CapZ", R_CapZ_TB);

            textboxes_Small.Add("A", SmallA_TB);
            textboxes_Small.Add("B", SmallB_TB);
            textboxes_Small.Add("C", SmallC_TB);
            textboxes_Small.Add("D", SmallD_TB);
            textboxes_Small.Add("E", SmallE_TB);
            textboxes_Small.Add("F", SmallF_TB);
            textboxes_Small.Add("G", SmallG_TB);
            textboxes_Small.Add("H", SmallH_TB);
            textboxes_Small.Add("I", SmallI_TB);
            textboxes_Small.Add("J", SmallJ_TB);
            textboxes_Small.Add("K", SmallK_TB);
            textboxes_Small.Add("L", SmallL_TB);
            textboxes_Small.Add("M", SmallM_TB);
            textboxes_Small.Add("N", SmallN_TB);
            textboxes_Small.Add("O", SmallO_TB);
            textboxes_Small.Add("P", SmallP_TB);
            textboxes_Small.Add("Q", SmallQ_TB);
            textboxes_Small.Add("R", SmallR_TB);
            textboxes_Small.Add("S", SmallS_TB);
            textboxes_Small.Add("T", SmallT_TB);
            textboxes_Small.Add("U", SmallU_TB);
            textboxes_Small.Add("V", SmallV_TB);
            textboxes_Small.Add("W", SmallW_TB);
            textboxes_Small.Add("X", SmallX_TB);
            textboxes_Small.Add("Y", SmallY_TB);
            textboxes_Small.Add("Z", SmallZ_TB);
                        
            textboxes_Num.Add("D0", Num0_TB);
            textboxes_Num.Add("D1", Num1_TB);
            textboxes_Num.Add("D2", Num2_TB);
            textboxes_Num.Add("D3", Num3_TB);
            textboxes_Num.Add("D4", Num4_TB);
            textboxes_Num.Add("D5", Num5_TB);
            textboxes_Num.Add("D6", Num6_TB);
            textboxes_Num.Add("D7", Num7_TB);
            textboxes_Num.Add("D8", Num8_TB);
            textboxes_Num.Add("D9", Num9_TB);

            textboxes_NumBlock.Add("NumPad0", NumBlock0_TB);
            textboxes_NumBlock.Add("NumPad1", NumBlock1_TB);
            textboxes_NumBlock.Add("NumPad2", NumBlock2_TB);
            textboxes_NumBlock.Add("NumPad3", NumBlock3_TB);
            textboxes_NumBlock.Add("NumPad4", NumBlock4_TB);
            textboxes_NumBlock.Add("NumPad5", NumBlock5_TB);
            textboxes_NumBlock.Add("NumPad6", NumBlock6_TB);
            textboxes_NumBlock.Add("NumPad7", NumBlock7_TB);
            textboxes_NumBlock.Add("NumPad8", NumBlock8_TB);
            textboxes_NumBlock.Add("NumPad9", NumBlock9_TB);
            textboxes_NumBlock.Add("Divide", NumBlockDivide_TB); 
            textboxes_NumBlock.Add("Subtract", NumBlockMinus_TB);
            textboxes_NumBlock.Add("Add", NumBlockPlus_TB);
            textboxes_NumBlock.Add("Enter", NumBlockEnter_TB);
            textboxes_NumBlock.Add("NumLock", NumBlockNL_TB); 
            textboxes_NumBlock.Add("Decimal", NumBlockDot_TB);
            textboxes_NumBlock.Add("Multiply", NumBlockMulti_TB);

            textboxes_PMarks.Add("OemComma", OemComma_TB);
            textboxes_PMarks.Add("OemPeriod", OemPeriod_TB);
            textboxes_PMarks.Add("Oem1", Oem1_TB);
            textboxes_PMarks.Add("Oem3", Oem3_TB);
            textboxes_PMarks.Add("Oem5", Oem5_TB);
            textboxes_PMarks.Add("Oem6", Oem6_TB);
            textboxes_PMarks.Add("Oem8", Oem8_TB);
            textboxes_PMarks.Add("OemQuestion", OemQuestion_TB);
            textboxes_PMarks.Add("OemMinus", OemMinus_TB);
            textboxes_PMarks.Add("OemPlus", OemPlus_TB);
            textboxes_PMarks.Add("OemOpenBrackets", OemOpenBrackets_TB);
            textboxes_PMarks.Add("OemQuotes", OemQuotes_TB);
            textboxes_PMarks.Add("Space", Space_TB);

            textboxes_F.Add("F1", F1_TB);
            textboxes_F.Add("F2", F2_TB);
            textboxes_F.Add("F3", F3_TB);
            textboxes_F.Add("F4", F4_TB);
            textboxes_F.Add("F5", F5_TB);
            textboxes_F.Add("F6", F6_TB);
            textboxes_F.Add("F7", F7_TB);
            textboxes_F.Add("F8", F8_TB);
            textboxes_F.Add("F9", F9_TB);
            textboxes_F.Add("F10", F10_TB);
            textboxes_F.Add("F11", F11_TB);
            textboxes_F.Add("F12", F12_TB);
            textboxes_F.Add("F13", F13_TB);
            textboxes_F.Add("F14", F14_TB);
            textboxes_F.Add("F15", F15_TB);
            textboxes_F.Add("F16", F16_TB);
            textboxes_F.Add("F17", F17_TB);
            textboxes_F.Add("F18", F18_TB);
            textboxes_F.Add("F19", F19_TB);

            textboxes_System.Add("Escape",Escape_TB);
            textboxes_System.Add("Snapshot", PrintScreen_TB);
            textboxes_System.Add("Scroll", Scroll_TB);

            textboxes_System.Add("Pause", Pause_TB); 
            textboxes_System.Add("Tab", Tab_TB);
            textboxes_System.Add("Capital", Capital_TB);
            textboxes_System.Add("LeftShift", LeftShift_TB);
            textboxes_System.Add("RightShift", RightShift_TB);
            textboxes_System.Add("Back", Back_TB);
            textboxes_System.Add("Return", Return_TB);
            textboxes_System.Add("LeftCtrl", LeftCtrl_TB);

            textboxes_System.Add("RightCtrl", RightCtrl_TB);
            textboxes_System.Add("LWin", LWin_TB);
            textboxes_System.Add("RWin", RWin_TB);
            textboxes_System.Add("Alt", Alt_TB);
            textboxes_System.Add("AltGr", AltGr_TB);
            textboxes_System.Add("Apps", Apps_TB);
            textboxes_System.Add("Insert", Insert_TB);
            textboxes_System.Add("Home", Home_TB);
            textboxes_System.Add("PageUp", PageUp_TB);
            textboxes_System.Add("Next", Next_TB);
            textboxes_System.Add("Delete", Delete_TB);
            textboxes_System.Add("End", End_TB);
            textboxes_System.Add("Up", Up_TB);
            textboxes_System.Add("Left", Left_TB);
            textboxes_System.Add("Right", Right_TB);
            textboxes_System.Add("Down", Down_TB);
        }

        public bool SysKeysChecked()
        {
            if (kb_keys["Home"] == true &&
                kb_keys["Delete"] == true &&
                kb_keys["PageUp"] == true &&
                kb_keys["Next"] == true &&
                kb_keys["End"] == true)
                return true;
            else return false;
        }

        private static bool IsExtended(KeyEventArgs e)
        {
            return (bool)typeof(KeyEventArgs).InvokeMember("IsExtendedKey", BindingFlags.GetProperty | BindingFlags.NonPublic | 
                BindingFlags.Instance, null, e, null);
        }
    }    
}
